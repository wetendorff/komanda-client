import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      locales: ['en', 'da'],
      locale: 'da',
      user: null,
    },
    mutations: {
      setLanguage(state, locale) {
        if (state.locales.indexOf(locale) !== -1) {
          state.locale = locale
        }
      },
      setUser(state, user) {
        state.user = user;
      }
    },
    actions: {
      async login(vuexContext, credentials) {
        const result = await this.$axios.$post('http://localhost:3000/api/v1/users/login', credentials);
        localStorage.setItem('user', result.user);
        vuexContext.commit('setUser', result.user);
      },
      async signup(vuexContext, data) {
        const result = await this.$axios.$post('http://localhost:3000/api/v1/users', data);
        vuexContext.commit('setUser', result.user);
      }
    },
    getters: {
      isAuthenticated(state) {
        return state.user != null;
      }
    }
  })
}

export default createStore;